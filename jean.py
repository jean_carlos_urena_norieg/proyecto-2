PANAMA = ["Balboa", "Chimán", "Chepo", "Taboga", "Panamá", "San Miguelito"]
DARIEN = ["Chepigana", "Pinogana", "Santa Fe"]
HERRERA = ["Chitré", "Las Minas", "Los Pozos", "Ocú", "Parita", "Santa María", "Pesé"]
BOCASDELTORO = ["Bocas del Toro", "Changuinola", "Chiriquí Grande", "Almirante"]
COCLE = ["Aguadulce", "Antón", "La Pintada", "Natá", "Olá", "Penonomé"]
COLON = ["Colón", "Chagres", "Donoso", "Portobelo", "Santa Isabel", "Omar Torrijos Herrera"]
PANAMAOESTE = ["Arraiján", "Capira", "Chame", "San Carlos", "La Chorrera"]
CHIRIQUI = ["Alanje", "Barú", "Boquerón", "Boquete", "Bugaba", "David", "Dolega", "Gualaca", "Remedios", "Renacimiento","San Félix", "Tierras Altas", "San Lorenzo", "Tolé"]
LOSSANTOS = ["Guararé", "Las Tablas", "Los Santos", "Macaracas", "Pedasí", "Pocrí", "Tonosí"]
VERAGUAS = ["Atalaya", "Calobre", "Cañazas", "La Mesa", "Las Palmas", "Mariato", "Montijo", "Río de Jesús","San Francisco", "Santa Fe", "Santiago", "Soná"]
provilista = {"Bocas del Toro" : BOCASDELTORO,"Coclé" : COCLE,"Colón" : COLON,"Chiriquí" : CHIRIQUI,"Darién" : DARIEN,"Herrera" : HERRERA,"Los Santos" : LOSSANTOS,"Panamá" : PANAMA,"Veraguas" : VERAGUAS,"Panamá Oeste" : PANAMAOESTE}
provi = 1
while provi != "Cerrar": 
    provi = input ("Introdusca el nombre de la Provincia con mayusculas y tildes: ")
        try: 
            print(provilista[provi]) 
        except: 
            if provi == "Cerrar": 
                exit()
            print("No se encontro. Revise que este bien escrito. ")
